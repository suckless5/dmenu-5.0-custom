# dmenu-5.0-custom
Custom configured dmenu_run program to look pretty and easy on the eyes. 

## Screenshot
<img src="/screenshot/custom-dmenu.png">

## Ubuntu dependencies
<ol>
  <li> build-essential </li>
  <li> libx11-dev </li>
  <li> libxft-dev </li>
  <li> libxinerama-dev </li>

</ol>

## Arch dependencies
<ol>
  <li> base-dev </li>
  <li> libx11 </li>
  <li> libxft </li>
</ol>

### Dmenu Theme
***Dracula***<br>
https://github.com/dracula/dracula-theme <br>

### Font size
#### Type: *monospace*

## dwm patches
<ol>
  <li> fuzzyhighlight: https://tools.suckless.org/dmenu/patches/fuzzyhighlight/ </li>
</ol>
